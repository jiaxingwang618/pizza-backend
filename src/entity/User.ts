import {Entity, ObjectIdColumn, ObjectID, Column, UpdateDateColumn, Unique} from "typeorm";
import {IsNotEmpty, Length} from "class-validator";
import  * as bycrypt from 'bcryptjs'
//RBAC role based access cpntrol
@Entity()
export class User {

    @ObjectIdColumn()
    id: ObjectID;

    @Column()
    @Length(4,20)
    username: string;

    @Column()
    @Length(4,20)
    password: string;

    @Column()
    @IsNotEmpty()
    role: string;

    @Column()
    @UpdateDateColumn()
    updateAt:Date;

    hashPassword() {
        this.password = bycrypt.hashSync(this.password , 8)
    }

    checkIfUnecnyptedPasswordValid(unencryptedPassword: string) {
        return bycrypt.compareSync(unencryptedPassword , this.password)
    }

}
