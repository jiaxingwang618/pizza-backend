import {getRepository} from "typeorm";
import {NextFunction, Request, Response} from "express";
import {User} from "../entity/User";
import {validate} from "class-validator";
const columFilter: any = {
    select: ['id' , 'username' , 'role']
}
export class UserController {

    // private static userRepository = getRepository(User);
    private static  get repo() {
        return getRepository(User)
    }

    static listAll = async (req: Request, res: Response) => {
        //Get users from DB
        // const userRepo = getRepository(User)
        const users = await UserController.repo.find({})

        res.send(users)
    }

    static getOneById = async (req: Request, res: Response) => {
        const id: string = req.params.uid
        try {
            const user = await UserController.repo.findOneOrFail(id, columFilter)
            res.send(user)
        } catch (e) {
            console.log('Expectionn')
            return res.status(404).send('User not find')
        }

    }

    static newUser = async (req: Request, res: Response) => {
        let {username , password , role } = req.body
        let user = new User()
        user.username  = username
        user.password = password
        user.role = role

        const  errors = await validate(user)
        if(errors.length>0){return res.status(400).send(errors) }

        user.hashPassword()

        try{
            await UserController.repo.save(user)
        }catch (e) {
            console.log('error', e)
            return  res.status(400).send('Save user error')
        }

        return res.status(201).send('Create User ' + username)
    }

    static editUser = async (req: Request , res: Response) => {
        const  id = req.params.uid
        let {username, role} = req.body
        let user: User = null
        try{
            user = await UserController.repo.findOneOrFail(id)
        }catch (e) {
           return res.status(404).send('User not find')
        }

        user.username = username
        user.role = role
        const  errors = await validate(user)
        if(errors.length > 0) {
            return res.status(400).send(errors)
        }

        try{
            await UserController.repo.save(user)
        }catch (e) {
           return res.status(409).send('username already in use'+ username)
        }

        return res.status(204).send()
    }

    static deleteUser = async (req: Request , res: Response) => {
        const  id = req.params.id
        try{
            await UserController.repo.findOneOrFail(id)
        }
        catch (e) {
           return res.status(404).send('User not found')
        }

        await UserController.repo.delete(id)
        return res.status(204).send('Delete successfully')
    }

}
