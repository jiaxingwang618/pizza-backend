import {getRepository} from "typeorm";
import {User} from "../entity/User";
import {Request, Response} from "express";
import * as jwt from 'jsonwebtoken'
import config from "../config/config";
class AuthController {
    private static  get repo() {
        return getRepository(User)
    }

    static login = async (req: Request , res: Response) => {
        // Check user name and password set
        let {username , password} = req.body
        if(!username && password) {
            res.status(400).send( {msg: 'Bad parameters'})
        }
        //Get the corresponding user from db
        let user :User = null
        try{
            user = await AuthController.repo.findOneOrFail({where:(username)})
        }catch (e) {
            return res.status(401).send({msg:'Can not find user'})
        }
        if(!user.checkIfUnecnyptedPasswordValid(password)){return res.status(401).send()}

        //SSO single sign on
        //todo

        const token = jwt.sign({userId: user.id , username: user.username , role: user.role} , config.jwtSecret,{'expiresIn': '1h'})

        return res.send(token)

    }

    static changePassword = async (req: Request , res: Response) => {

    }
}
