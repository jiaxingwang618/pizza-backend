import {Router} from "express";
import user from './user'
// import AuthController from

const routes = Router()

//login

routes.use('/login', AuthController.login)
//Change my password
routes.use('/change-password',AuthController.changePassword)


export default routes
