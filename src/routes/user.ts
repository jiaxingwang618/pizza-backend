import {Router} from "express";
import {UserController} from "../controller/UserController";

const regxUser = '[a-z0-9]+'
const router = Router()


//Get all users

router.get('/' , UserController.listAll)


//get one user

router.get(`/:uid`, UserController.getOneById)


//Create a user
router.post('/' , UserController.newUser)

//Update User
router.patch(`/:uid(${regxUser})` , UserController.editUser)

//Delete User
router.delete(`/:uid(${regxUser})` , UserController.deleteUser)


export default router
